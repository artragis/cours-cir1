<?php


function openDoor()
{
    if (isThereALock()) {
        openLock();
    }
    if (thereIsADigicode()) {
        /// blabla
    }
    pushTheDoor();
}

/* fonctions internes */
function isThereALock()
{

}

function openLock()
{

}


interface LockableSystem
{
    function isCompatibleWithDoor();

    function unlockSystem();
}

class DigicodeLocker implements LockableSystem
{
    private $pass;

    public function __construct($pass)
    {
        $this->pass = $pass;
    }

    function isCompatibleWithDoor()
    {
        return true;
    }

    function unlockSystem()
    {
        usePassWord($this->pass);
    }
}


class RobotRunner
{
    private $lockableSystems;

    public function __construct()
    {
        $this->lockableSystems = [];
    }

    function avancer()
    {

    }

    function atEnd()
    {

    }

    function thereIsADoor()
    {

    }

    function addLockableSystem(LockableSystem $system)
    {
        $this->locableSystems[] = $system;
    }

    function openDoor()
    {
        foreach ($this->locakbleSystems as $system) {
            if ($system->isCompatibleWithDoor()) {
                $system->unlockSystem();
            }
        }
        pushTheDoor();
    }


}

$robot = new RobotRuner();
$robo->addLockableSystem(new DigicodeLocker("abcd"));

while (!$robot->atEnd()) {
    if ($robot->thereIsADoor()) {
        $robot->openDoor();
    }
    $robot->avancer();
}