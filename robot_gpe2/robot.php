<?php


function isThereADoor() {
    return true;
}

function isLockedWithOldLock() {

}
function unlockOldLock(){

}

interface Unlocker {
    function isCompatible(): bool;
    function unlock();
}

class DigicodeUnlocker implements Unlocker {
    private $code;
    public static function getLockersForCodes($code1, $code2){
        return [new DigicodeUnlocker($code1), new DigicodeUnlocker($code2)];
    }
    public function __construct($code)
    {
        $this->code = $code;
    }

    function isCompatible(): bool
    {
        return true;
    }

    function unlock()
    {
        // TODO: Implement unlock() method.
    }

}

class RobotRunner
{
    private $unlockers;
    public function __construct()
    {
        $this->unlockers = [];
    }

    function addUnlocker(Unlocker $unlocker) {
        $this->unlockers[] = $unlocker;
    }
    function isThereADoor() {
        return true;
    }

    function openTheDoor()
    {
        foreach ($this->unlockers as $unlocker) {
            if ($unlocker->isCompatible()) {
                $unlocker->unlock();
            }
        }
        // quelque chose
    }

    function avancer() {
        // quelque chose
    }

    function atEnd()
    {
        return false;
    }
}

$runner = new RobotRunner();
$runner->addUnlocker(new DigicodeUnlocker("123456"));
$runner->addUnlocker(new DigicodeUnlocker("42 is the answer"));
while(!$runner->atEnd()) {
    if ($runner->thereIsADoor()) {
        $runner->openTheDoor();
    }
    $runner->avancer();
}


